package org.easwaran.orderexpress.orderservice.util;

import org.easwaran.orderexpress.orderservice.model.Order;
import org.easwaran.orderexpress.orderservice.model.OrderItem;
import org.easwaran.orderexpress.orderservice.repository.OrderItemRepository;
import org.easwaran.orderexpress.orderservice.repository.OrderRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {

    final
    OrderRepository orderRepository;

    final
    OrderItemRepository orderItemRepository;

    public CommandLineAppStartupRunner(OrderRepository orderRepository, OrderItemRepository orderItemRepository) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
    }


    @Override
    public void run(String... args) {

        Order orderEntity = new Order();
        OrderItem orderItemEntity = new OrderItem();

        orderItemEntity.setItemId(1L);
        orderItemEntity.setProductName("Pixel 7 Pro");
        orderItemEntity.setQuantity(1);
        orderItemEntity.setPrice(970.34);

        orderEntity.setOrderId(1L);
        orderEntity.setCustomerName("Easwaran");
        orderEntity.setOrderItems(List.of(orderItemEntity));

        orderRepository.save(orderEntity);

    }
}