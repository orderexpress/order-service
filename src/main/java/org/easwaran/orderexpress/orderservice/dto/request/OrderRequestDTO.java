package org.easwaran.orderexpress.orderservice.dto.request;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Optional;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderRequestDTO implements Serializable {

    @NotNull(message = "Order ID can not be null")
    private Long orderId;

    @NotNull(message = "Customer Name can not be null")
    private String customerName;

    private List<OrderItemRequestDTO> orderItems;

    public Optional<List<OrderItemRequestDTO>> getOrderItems() {
        return Optional.ofNullable(orderItems);
    }
}
