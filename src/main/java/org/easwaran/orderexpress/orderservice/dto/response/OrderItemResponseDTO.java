package org.easwaran.orderexpress.orderservice.dto.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.util.List;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderItemResponseDTO implements Serializable {

    private Long itemID;

    private String productName;

    private int quantity;

    private double price;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<OrderResponseDTO> orderResponseDTOS;

}
