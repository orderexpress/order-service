package org.easwaran.orderexpress.orderservice.dto.request;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class OrderItemRequestDTO implements Serializable {

    @NotNull(message = "Order ID can not be null")
    private Long itemId;

    @NotNull(message = "Product Name can not be null")
    private String productName;

    @NotNull(message = "Quantity can not be null")
    private String quantity;

    @NotNull(message = "Price can not be null")
    private String price;
}
