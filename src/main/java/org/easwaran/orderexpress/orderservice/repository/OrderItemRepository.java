package org.easwaran.orderexpress.orderservice.repository;

import org.easwaran.orderexpress.orderservice.model.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {

}
