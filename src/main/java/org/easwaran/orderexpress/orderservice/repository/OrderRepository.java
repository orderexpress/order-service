package org.easwaran.orderexpress.orderservice.repository;

import org.easwaran.orderexpress.orderservice.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
