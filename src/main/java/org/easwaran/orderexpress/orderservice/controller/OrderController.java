package org.easwaran.orderexpress.orderservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.easwaran.orderexpress.orderservice.dto.request.OrderRequestDTO;
import org.easwaran.orderexpress.orderservice.model.Order;
import org.easwaran.orderexpress.orderservice.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@Slf4j
@Validated
@RequestMapping("/orders/v1")
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping
    public ResponseEntity<List<Order>> getAllOrders() {
        Optional<List<Order>> orderResponse = orderService.getAllOrders();
        return orderResponse.map(orderEntity -> new ResponseEntity<>(orderEntity, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<Order> getOrderById(@PathVariable(value = "orderId") Long orderId) {
        Optional<Order> orderResponse = orderService.getOrderById(orderId);
        return orderResponse.map(orderEntity -> new ResponseEntity<>(orderEntity, HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @PostMapping(path = "/save", consumes = {APPLICATION_JSON_VALUE}, produces = {APPLICATION_JSON_VALUE})
    public ResponseEntity<String> createOrder(@Valid @RequestBody List<OrderRequestDTO> orderRequestDTO) throws ParseException {
        orderService.saveOrder(orderRequestDTO);
        return new ResponseEntity<>(HttpStatus.OK);

    }
}
