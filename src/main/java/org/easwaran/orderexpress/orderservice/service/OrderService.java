package org.easwaran.orderexpress.orderservice.service;

import org.easwaran.orderexpress.orderservice.dto.request.OrderRequestDTO;
import org.easwaran.orderexpress.orderservice.model.Order;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;

public interface OrderService {
    Optional<Order> getOrderById(Long id);
    Optional<List<Order>> getAllOrders();
    void saveOrder(List<OrderRequestDTO> orderRequestDTOList) throws ParseException;
}
