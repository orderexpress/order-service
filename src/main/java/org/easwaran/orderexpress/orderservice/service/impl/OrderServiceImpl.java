package org.easwaran.orderexpress.orderservice.service.impl;

import lombok.RequiredArgsConstructor;
import org.easwaran.orderexpress.orderservice.dto.request.OrderItemRequestDTO;
import org.easwaran.orderexpress.orderservice.dto.request.OrderRequestDTO;
import org.easwaran.orderexpress.orderservice.model.Order;
import org.easwaran.orderexpress.orderservice.model.OrderItem;
import org.easwaran.orderexpress.orderservice.repository.OrderRepository;
import org.easwaran.orderexpress.orderservice.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    /**
     * @param id
     * @return
     */
    @Override
    public Optional<Order> getOrderById(Long id) {
        return orderRepository.findById(id);
    }

    /**
     * @return
     */
    @Override
    public Optional<List<Order>> getAllOrders() {
        List<Order> orderEntity = orderRepository.findAll();
        return Optional.of(orderEntity);
    }

    /**
     * @param orderRequestDTOList
     * @throws ParseException
     */
    @Override
    public void saveOrder(List<OrderRequestDTO> orderRequestDTOList) throws ParseException {
        List<Order> orders = orderRequestDTOList.stream()
                .map(this::mapToOrderEntity)
                .collect(Collectors.toList());
        orderRepository.saveAll(orders);
    }

    private Order mapToOrderEntity(OrderRequestDTO orderRequestDTO) {
        Order order = new Order();
        order.setOrderId(orderRequestDTO.getOrderId());
        order.setCustomerName(orderRequestDTO.getCustomerName());

        // Check if order items exist before mapping
        if (orderRequestDTO.getOrderItems().isPresent()) {
            List<OrderItem> orderItems = orderRequestDTO.getOrderItems().stream()
                    .map(orderItemRequestDTO -> mapToOrderItemEntity((OrderItemRequestDTO) orderItemRequestDTO))
                    .collect(Collectors.toList());
            order.setOrderItems(orderItems);
        }

        return order;
    }

    private OrderItem mapToOrderItemEntity(OrderItemRequestDTO orderItemRequestDTO) {
        OrderItem orderItem = new OrderItem();
        orderItem.setProductName(orderItemRequestDTO.getProductName());
        orderItem.setQuantity(Integer.parseInt(orderItemRequestDTO.getQuantity()));
        orderItem.setPrice(Double.parseDouble(orderItemRequestDTO.getPrice()));
        return orderItem;
    }
}
